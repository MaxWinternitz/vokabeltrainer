// Funktion zum Laden von Schulnoten
function loadGrades() {
    // Array von Noten definieren
    const grades = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
    // Container für die Notenbuttons abrufen
    const container = document.getElementById('gradeButtons');
    // Für jede Note einen Button erstellen und zum Container hinzufügen
    grades.forEach(grade => {
        const button = document.createElement('button');
        button.className = 'knopf';
        button.textContent = `Klasse ${grade}`;  // Text des Buttons auf Deutsch
        button.onclick = function() { selectGrade(grade); }; // Bei Klick die entsprechende Klasse auswählen
        container.appendChild(button);
    });
}

// Funktion zum Auswählen einer Schulnote
function selectGrade(grade) {
    // Ausgewählte Schulnote in sessionStorage speichern
    sessionStorage.setItem('selectedGrade', grade);
    // Zur Seite 'unitselection.html' weiterleiten
    window.location.href = 'unitselection.html';
}

// Definiert, was beim Laden des Fensters passiert
window.onload = function() {
    // Funktion loadGrades beim Laden der Seite aufrufen
    loadGrades(); 
};
