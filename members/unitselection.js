// Funktion zum Laden spezifischer Unterrichtseinheitendaten basierend auf der ausgewählten Klasse und Einheit
function loadUnit(unit) {
    // Die ausgewählte Klasse aus dem sessionStorage abrufen
    const grade = sessionStorage.getItem('selectedGrade');
    // Wenn keine Klasse ausgewählt wurde, den Benutzer benachrichtigen und die Funktion stoppen
    if (!grade) {
        alert('Keine Klasse ausgewählt. Bitte wählen Sie zuerst eine Klasse.');
        return;
    }

    // Den Dateipfad für die JSON-Datei des Vokabulars definieren
    const jsonFile = '/public/vocabulary.json';
    // Die JSON-Datei vom Server abrufen
    fetch(jsonFile)
        .then(response => {
            // Überprüfen, ob die Antwort nicht in Ordnung ist, und einen Fehler werfen
            if (!response.ok) {
                throw new Error('Netzwerkantwort war nicht in Ordnung');
            }
            // Das JSON aus der Antwort parsen
            return response.json();
        })
        .then(data => {
            // Die abgerufenen Daten zum Debuggen loggen
            console.log("Abgerufene Daten:", data);
            // Die Einheitennummer aus dem Funktionsargument parsen
            const unitNumber = parseInt(unit);
            // Loggen, welche Einheits-ID gesucht wird
            console.log("Suche nach Einheits-ID:", unitNumber);

            // Die entsprechenden Klassendaten aus den abgerufenen JSON-Daten finden
            const gradeData = data.grades.find(g => g.grade === parseInt(grade));
            // Wenn keine Klassendaten gefunden werden, einen Fehler werfen
            if (!gradeData) {
                throw new Error(`Klassendaten nicht gefunden für Klasse ${grade}`);
            }
            
            // Die spezifischen Einheitendaten innerhalb der Klassendaten finden
            const unitData = gradeData.units.find(u => u.unitId === unitNumber);
            // Wenn keine Einheitendaten gefunden werden, den Fehler loggen, den Benutzer benachrichtigen und die Funktion stoppen
            if (!unitData) {
                console.error('Einheitendaten nicht gefunden für Einheits-ID:', unitNumber);
                alert('Einheitendaten nicht gefunden. Bitte versuchen Sie eine andere Einheit.');
                return;
            }
            // Die gefundenen Einheitendaten zum Debuggen loggen
            console.log("Gefundene Einheitendaten:", unitData);

            // Die gefundenen Einheitendaten im sessionStorage speichern und zur Anwendungsseite weiterleiten
            sessionStorage.setItem('selectedUnitData', JSON.stringify(unitData));
            window.location.href = 'app.html';
        })
        .catch(error => {
            // Fehler beim Laden der JSON-Datei loggen und den Benutzer darüber benachrichtigen
            console.error('Fehler beim Laden der JSON-Datei:', error);
            alert('Daten konnten nicht geladen werden. Bitte versuchen Sie es erneut.');
        });
}

// Funktion zum Laden aller Einheiten basierend auf der ausgewählten Klasse
function loadUnits() {
    // Den Dateipfad für die JSON-Datei des Vokabulars definieren
    const jsonFile = '/public/vocabulary.json';
    // Die JSON-Datei vom Server abrufen
    fetch(jsonFile)
        .then(response => {
            // Überprüfen, ob die Antwort nicht in Ordnung ist, und einen Fehler werfen
            if (!response.ok) {
                throw new Error(`Netzwerkantwort war nicht in Ordnung, Status: ${response.status}`);
            }
            // Das JSON aus der Antwort parsen
            return response.json();
        })
        .then(data => {
            // Die abgerufenen Daten zum Debuggen loggen
            console.log("Abgerufene Daten:", data);
            // Überprüfen, ob die Klassendaten im abgerufenen JSON fehlen und einen Fehler werfen, falls ja
            if (!data.grades) {
                throw new Error('Klassendaten fehlen im abgerufenen JSON.');
            }

            // Die ausgewählte Klasse aus dem sessionStorage abrufen
            const grade = sessionStorage.getItem('selectedGrade');
            // Wenn keine Klasse ausgewählt wurde, einen Fehler werfen
            if (!grade) {
                throw new Error('Keine Klasse ausgewählt. Bitte wählen Sie zuerst eine Klasse.');
            }

            // Die entsprechenden Klassendaten aus den abgerufenen JSON-Daten finden
            const gradeData = data.grades.find(g => g.grade === parseInt(grade));
            // Wenn keine Klassendaten gefunden werden, einen Fehler werfen
            if (!gradeData) {
                throw new Error(`Klassendaten nicht gefunden für Klasse ${grade}`);
            }

            // Das HTML-Container-Element abrufen, in dem die Einheitsbuttons angezeigt werden
            const container = document.getElementById('unitButtons');
            // Wenn der Container nicht gefunden wird, einen Fehler werfen
            if (!container) {
                throw new Error('Container für Einheitsbuttons nicht gefunden.');
            }
            // Vorhandenen Inhalt im Container löschen
            container.innerHTML = '';
            // Für jede Einheit in den Klassendaten eine Schaltfläche erstellen
            gradeData.units.forEach(unit => {
                const button = document.createElement('button');
                button.textContent = `Unit ${unit.unitId}`;
                button.className = 'knopf';
                // Einen Klick-Handler für die Schaltfläche festlegen, um die spezifische Einheit zu laden
                button.onclick = function() { loadUnit(unit.unitId); };
                // Die Schaltfläche zum Container hinzufügen
                container.appendChild(button);
            });
        })
        .catch(error => {
            // Fehler beim Laden der Einheiten loggen und den Benutzer darüber benachrichtigen
            console.error('Fehler beim Laden der Einheiten:', error);
            alert(`Fehler beim Laden der Einheitendaten: ${error.message}`);
        });
}

// Definiert, was passiert, wenn das Fenster geladen wird
window.onload = function() {
    // Die Funktion loadUnits aufrufen, um alle Einheiten beim Laden des Fensters zu laden
    loadUnits();
};

// Funktion zum Navigieren zum Hauptmenü
function goToMainMenu() {
    window.location.href = 'mainmenu.html';
}
