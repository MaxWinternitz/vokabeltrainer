// Variablen für die Vokabelliste, die aktuelle Spracheinstellung, den aktuellen Wortindex und die aktive Benutzer-ID deklarieren
let vocabulary = [];
let currentLanguage = 'ENtoDE';
let currentWordIndex = 0;
let userEmail = sessionStorage.getItem('userEmail');
// Aktive Benutzer-ID im aktivenUserId speichern
let activeUserId = userEmail;

// Funktion zum Abrufen von Benutzerdaten aus dem lokalen Speicher
function getUserData(userId) {
    // Benutzerdatenelement aus dem localStorage abrufen
    const userData = localStorage.getItem(userId);
    // Benutzerdaten parsen und zurückgeben, falls vorhanden, sonst null zurückgeben
    return userData ? JSON.parse(userData) : null;
}

// Funktion zum Speichern von Benutzerdaten im lokalen Speicher
function saveUserData(userId, userData) {
    // Benutzerdatenelement in localStorage als Zeichenkette speichern
    localStorage.setItem(userId, JSON.stringify(userData));
}

// Definieren, was passiert, wenn das Fenster geladen wird
window.onload = function() {
    // Einheitendaten aus dem sessionStorage abrufen
    let storedUnitData = sessionStorage.getItem('selectedUnitData');
    // Überprüfen, ob Einheitendaten vorhanden sind
    if (storedUnitData) {
        // Die gespeicherten Einheitendaten parsen
        let unitData = JSON.parse(storedUnitData);
        // Vokabular mit Einheitendaten initialisieren und Zählungen für jede Sprache zurücksetzen
        vocabulary = unitData.vocabulary.map(vocab => ({
            ...vocab,
            enCount: 0, 
            deCount: 0
        }));
        // Ein neues Wort aus dem Vokabular anzeigen
        displayNewWord();
    } else {
        // Fehler loggen, wenn keine Einheitendaten in sessionStorage gefunden werden
        console.error('No unit data found in sessionStorage');
    }
};

// Funktion zum Wechseln zwischen Sprachrichtungen
function toggleLanguage() {
    // Aktuelle Sprache zwischen 'ENtoDE' und 'DEtoEN' umschalten
    currentLanguage = (currentLanguage === 'ENtoDE') ? 'DEtoEN' : 'ENtoDE';
    // Sprachanzeigetext basierend auf der aktuellen Sprache aktualisieren
    document.getElementById('currentLanguage').innerText = currentLanguage === 'ENtoDE' ? 'Aktuell: Englisch zu Deutsch' : 'Aktuell: Deutsch zu Englisch';
    // Ein neues Wort basierend auf der neuen Spracheinstellung anzeigen
    displayNewWord();
}

// Funktion zum Anzeigen eines neuen Wortes zur Übersetzung
function displayNewWord() {
    // Vokabular filtern, um Wörter einzubeziehen, die in keiner Sprache eine Zählung von 3 erreicht haben
    let availableVocabulary = vocabulary.filter(word => Math.max(word.enCount, word.deCount) < 3);
    // Überprüfen, ob noch Wörter zum Übersetzen übrig sind
    if (availableVocabulary.length === 0) {
        // UI aktualisieren, um Abschlussnachricht anzuzeigen und Eingabefelder auszublenden
        document.getElementById('wordToTranslate').innerText = "Herzlichen Glückwunsch! Alle Vokabeln wurden gemeistert.";
        document.getElementById('userInput').style.display = 'none';
        document.getElementById('feedback').innerText = '';
        return;
    }

    // Ein zufälliges Wort aus dem verfügbaren Vokabular auswählen
    let randomIndex = Math.floor(Math.random() * availableVocabulary.length);
    // Aktuellen Wortindex basierend auf ausgewähltem Wort aktualisieren
    currentWordIndex = vocabulary.indexOf(availableVocabulary[randomIndex]);
    // Das Wort in der aktuellen Sprache anzeigen
    document.getElementById('wordToTranslate').innerText =
        currentLanguage === 'ENtoDE' ? availableVocabulary[randomIndex].en : availableVocabulary[randomIndex].de;
}

// Funktion zum Überprüfen der Antwort des Benutzers gegen die korrekte Übersetzung
function checkAnswer() {
    // Benutzereingabe abrufen
    const userAnswer = document.getElementById('userInput').value.trim().toLowerCase();

    // Aktuelles Wort basierend auf Index abrufen
    const currentWord = vocabulary[currentWordIndex];

    // Korrekte Antworten basierend auf der aktuellen Sprache bestimmen
    let correctAnswers = currentLanguage === 'ENtoDE' ? currentWord.de : currentWord.en;

    // Sicherstellen, dass correctAnswers ein Array ist (hilfreich, wenn die Daten nicht einheitlich sind)
    if (!Array.isArray(correctAnswers)) {
        correctAnswers = [correctAnswers];  // In ein Array umwandeln, falls noch nicht geschehen
    }

    // Überprüfen, ob die Benutzereingabe mit einer der korrekten Antworten übereinstimmt
    const isCorrect = correctAnswers.some(answer => answer.toLowerCase() === userAnswer);

    // UI basierend darauf aktualisieren, ob die Benutzereingabe korrekt ist
    if (isCorrect) {
        document.getElementById('feedback').innerText = "Richtig!";
        updateVocabularyProgress(activeUserId, currentWord.id, true, currentLanguage);
    } else {
        document.getElementById('feedback').innerText = `Falsch, eine mögliche richtige Antwort ist ${correctAnswers[0]}`;
        updateVocabularyProgress(activeUserId, currentWord.id, false, currentLanguage);
    }

    // Eingabefeld leeren und ein neues Wort anzeigen
    document.getElementById('userInput').value = '';
    displayNewWord();
}
function updateVocabularyProgress(userId, vocabId, correct, language) {
    fetch('http://localhost:7777/updateVocabularyProgress', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            userId: userId,
            vocabId: vocabId,
            language: language,
            correct: correct
        })
    })
    .then(response => response.json())
    .then(data => {
        console.log('Progress updated:', data);
    })
    .catch(error => {
        console.error('Failed to update vocabulary progress:', error);
    });
}

function goBack() {
    window.history.back();
};
function checkSentence() {
    const llmInput = document.getElementById('llm').value;
    const vocabularyWord = document.getElementById('wordToTranslate').innerText;

    // Überprüfen, ob tatsächlich eine Eingabe und Vokabular zum Senden vorhanden sind
    if (!llmInput.trim() || !vocabularyWord) {
        document.getElementById('llmResponse').textContent = "Bitte geben Sie einen Satz ein und stellen Sie sicher, dass Vokabular verfügbar ist.";
        return;
    }

    const payload = {
        llmMessage: llmInput,
        vocabulary: vocabularyWord // Geändert von Vokabular-Array zu einzelnen Vokabularworten
    };

    console.log("Sending to LLM:", payload);

    fetch('/api/message', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(payload)
    })
    .then(response => {
        if (!response.ok) {
            throw new Error('Network response was not ok');
        }
        return response.json();
    })
    .then(data => {
        document.getElementById('llmResponse').textContent = data.reply;
    })
    .catch(error => {
        console.error('Error sending message to LLM:', error);
        document.getElementById('llmResponse').textContent = 'Fehler beim Senden der Nachricht: ' + error.message;
    });
}
