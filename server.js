const express = require('express');
const session = require('express-session');
const bcrypt = require('bcrypt');
const path = require('path');
const cors = require('cors');
const bodyParser = require('body-parser');
const app = express();
const PORT = 7777;

app.use(bodyParser.json());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());
app.use(session({
    secret: '513423sdgsh234treh4',
    resave: false,
    saveUninitialized: true,
    cookie: { secure: false }
}));

const messageRoutes = require('./routes/messageRoutes');
const userRoutes = require('./routes/userRoutes');
const vocabularyRoutes = require('./routes/vocabularyRoutes');

app.use(messageRoutes);
app.use(userRoutes);
app.use(vocabularyRoutes);

app.use(express.static(path.join(__dirname, 'public')));
app.use('/public', express.static(path.join(__dirname, 'public')));
app.use('/members', express.static(path.join(__dirname, 'members')));

app.listen(PORT, () => {
    console.log(`Server running on http://localhost:${PORT}`);
});
