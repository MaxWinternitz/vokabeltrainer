const express = require('express');
const router = express.Router();
const OpenAI = require('openai');

const openai = new OpenAI({
    apiKey: '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',
});

router.post('/api/message', async (req, res) => {
    const { llmMessage, vocabulary } = req.body;
    console.log("Englisches Wort:", vocabulary);
    const systemMessage = `Prüfen, ob der Benutzer einen !englischen! Satz mit dem Wort "${vocabulary}" geschrieben hat. Überprüfen Sie den Satz auf Rechtschreib- oder Grammatikfehler und geben Sie dem Benutzer den korrigierten Satz. Wenn der Satz größtenteils richtig ist, sagen Sie dem Benutzer, dass er gute Arbeit geleistet hat. Antworten Sie sonst nichts.`;

    try {
        const chatCompletion = await openai.chat.completions.create({
            messages: [
                { role: 'system', content: systemMessage },
                { role: 'user', content: llmMessage },
            ],
            model: 'gpt-4-turbo',
        });

        const reply = chatCompletion.choices[0]?.message?.content || '';
        res.json({ reply });
    } catch (error) {
        console.error('Fehler beim Abrufen der Antwort von OpenAI:', error);
        res.status(500).json({ error: 'Fehler bei der Kommunikation mit OpenAI' });
    }
});

module.exports = router;
