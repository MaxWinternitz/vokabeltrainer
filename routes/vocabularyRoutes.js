const express = require('express');
const router = express.Router();
const userModel = require('../models/userModel');

router.post('/updateVocabularyProgress', async (req, res) => {
    const { userId, vocabId, language, correct } = req.body;
    console.log(`Empfangene Daten: userId=${userId}, vocabId=${vocabId}, language=${language}, correct=${correct}`);
    try {
        const success = await userModel.updateVocabularyProgress(userId, vocabId, language, correct);
        if (!success) {
            return res.status(404).json({ message: "Benutzer nicht gefunden oder Fehler beim Aktualisieren" });
        }
        res.json({ message: "Vokabularfortschritt erfolgreich aktualisiert" });
    } catch (error) {
        console.error('Fehler beim Aktualisieren des Vokabularfortschritts:', error);
        res.status(500).json({ message: "Fehler beim Aktualisieren des Vokabularfortschritts" });
    }
});

module.exports = router;
