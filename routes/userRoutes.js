const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const userModel = require('../models/userModel');

router.post('/register', async (req, res) => {
    const { email, password, vocabularyProgress} = req.body;
    try {
        if (await userModel.isUserExist(email)) {
            return res.status(400).json({ success: false, message: "E-Mail bereits registriert" });
        }

        const hashedPassword = await bcrypt.hash(password, 10);
        await userModel.saveUser({ email, password: hashedPassword, vocabularyProgress: {}});
        res.status(200).json({ success: true, message: "Registrierung erfolgreich!" });
    } catch (error) {
        console.error('Fehler:', error.stack);
        res.status(500).json({ success: false, message: "Serverfehler" });
    }
});

router.post('/login', async (req, res) => {
    const { email, password } = req.body;
    try {
        const user = await userModel.getUserByEmail(email);
        if (user && await bcrypt.compare(password, user.password)) {
            req.session.userEmail = email;  // E-Mail in der Session speichern
            res.json({ success: true, message: 'Anmeldung erfolgreich!' });
        } else {
            res.status(401).json({ success: false, message: 'Ungültige Anmeldedaten' });
        }
    } catch (error) {
        console.error('Fehler:', error);
        res.status(500).json({ success: false, message: "Serverfehler" });
    }
});

router.post('/logout', (req, res) => {
    req.session.destroy((err) => {
        if (err) {
            return res.status(500).json({ success: false, message: 'Fehler beim Abmelden' });
        }
        res.json({ success: true, message: 'Erfolgreich abgemeldet!' });
    });
});

module.exports = router;
