const fs = require('fs');
const path = require('path');
const usersFilePath = path.join(__dirname, '../users.json');

async function readUserData() {
    return new Promise((resolve, reject) => {
        fs.readFile(usersFilePath, 'utf8', (err, data) => {
            if (err) {
                reject(err);
            } else {
                try {
                    resolve(JSON.parse(data));
                } catch (parseError) {
                    reject(parseError);
                }
            }
        });
    });
}

async function saveUserData(users) {
    return new Promise((resolve, reject) => {
        fs.writeFile(usersFilePath, JSON.stringify(users, null, 2), 'utf8', (err) => {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        });
    });
}

async function isUserExist(email) {
    const users = await readUserData();
    return !!users[email];
}

async function saveUser(user) {
    const users = await readUserData();
    users[user.email] = user;
    await saveUserData(users);
}

async function getUserByEmail(email) {
    const users = await readUserData();
    return users[email];
}

async function updateVocabularyProgress(userId, vocabId, language, correct) {
    try {
        const userData = await readUserData();
        if (!userData || !userData[userId]) {
            console.error("User data not found for ID:", userId);
            return false;
        }

        if (!userData[userId].vocabularyProgress) {
            userData[userId].vocabularyProgress = {};
        }

        if (!userData[userId].vocabularyProgress[vocabId]) {
            userData[userId].vocabularyProgress[vocabId] = { deCount: 0, enCount: 0 };
        }

        let countKey = language === 'ENtoDE' ? 'deCount' : 'enCount';
        if (correct) {
            userData[userId].vocabularyProgress[vocabId][countKey] = Math.min(userData[userId].vocabularyProgress[vocabId][countKey] + 1, 3);
        } else {
            userData[userId].vocabularyProgress[vocabId][countKey] = Math.max(userData[userId].vocabularyProgress[vocabId][countKey] - 1, 0);
        }

        await saveUserData(userData);
        return true;
    } catch (error) {
        console.error("Error updating vocabulary progress:", error);
        return false;
    }
}


module.exports = {
    isUserExist,
    saveUser,
    getUserByEmail,
    updateVocabularyProgress
};
