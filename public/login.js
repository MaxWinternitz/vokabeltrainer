document.getElementById('loginForm').addEventListener('submit', async function(event) {
    event.preventDefault(); // Verhindert das Standardverhalten des Formulars beim Senden

    var email = document.getElementById('email').value; // E-Mail aus dem Formular abrufen
    var password = document.getElementById('password').value; // Passwort aus dem Formular abrufen

    try {
        // Asynchroner POST-Request an den Server senden
        const response = await fetch('http://localhost:7777/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json', // Setzt den Inhaltstyp auf JSON
            },
            body: JSON.stringify({ email: email, password: password }) // Sendet E-Mail und Passwort im JSON-Format
        });

        const data = await response.json(); // Antwort des Servers als JSON verarbeiten

        if (response.ok) {
            // E-Mail im sessionStorage speichern nach erfolgreichem Login
            sessionStorage.setItem('userEmail', email);
            // Weiterleitung zur Hauptmenüseite nach erfolgreichem Login
            window.location.href = '/members/mainmenu.html';
        } else {
            // Wirft einen Fehler mit der Nachricht aus der Antwort, wenn die Anmeldung nicht erfolgreich war
            throw new Error(data.message);
        }
    } catch (error) {
        // Loggt Fehler in der Konsole, wenn ein Problem mit der Anfrage auftritt
        console.error('Fehler:', error);
    }
});
