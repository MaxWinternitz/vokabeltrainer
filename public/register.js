document.getElementById('registerForm').addEventListener('submit', async function(event) {
    event.preventDefault();  // Verhindert das Standardverhalten des Formulars beim Senden

    var email = document.getElementById('email').value; // E-Mail aus dem Formular abrufen
    var password = document.getElementById('password').value; // Passwort aus dem Formular abrufen

    try {
        // Asynchroner POST-Request an den Server zum Registrieren senden
        const response = await fetch('http://localhost:7777/register', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json', // Setzt den Inhaltstyp auf JSON
            },
            body: JSON.stringify({ email: email, password: password, vocabularyProgress: {} }) // Sendet E-Mail und Passwort im JSON-Format
        });

        if (!response.ok) {
            // Überprüft den Inhaltstyp der Antwort und verarbeitet die Nachricht entsprechend
            if (response.headers.get("content-type")?.includes("application/json")) {
                const data = await response.json();
                throw new Error(data.message);
            } else {
                const text = await response.text();
                throw new Error(text);
            }
        }

        const data = await response.json(); // Antwort des Servers als JSON verarbeiten
        if (data.success) {
            // Benutzer über erfolgreiche Registrierung informieren und zur Anmeldeseite weiterleiten
            alert('Registrierung erfolgreich! Bitte melden Sie sich an.');
            window.location.href = 'index.html'; 
        } else {
            // Wirft einen Fehler mit der Nachricht aus der Antwort, wenn die Registrierung nicht erfolgreich war
            throw new Error(data.message); 
        }
    } catch (error) {
        // Loggt Fehler in der Konsole und benachrichtigt den Benutzer, wenn die Registrierung fehlschlägt
        console.error('Fehler:', error);
        alert('Registrierung fehlgeschlagen: ' + error.message);
    }
});
