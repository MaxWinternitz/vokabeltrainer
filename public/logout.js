document.getElementById('logoutButton').addEventListener('click', async function() {
    try {
        // Asynchroner POST-Request an den Server zum Abmelden senden
        const response = await fetch('http://localhost:7777/logout', {
            method: 'POST' // Setzt die HTTP-Methode auf POST
        });

        const data = await response.json(); // Antwort des Servers als JSON verarbeiten
        if (response.ok) {
            // Benutzer über erfolgreiche Abmeldung informieren
            alert('Abmeldung erfolgreich!');
            // Weiterleitung zur Startseite nach erfolgreicher Abmeldung
            window.location.href = '/public/index.html'; 
        } else {
            // Wirft einen Fehler mit der Nachricht aus der Antwort, wenn die Abmeldung nicht erfolgreich war
            throw new Error(data.message);
        }
    } catch (error) {
        // Loggt Fehler in der Konsole und benachrichtigt den Benutzer, wenn die Abmeldung fehlschlägt
        console.error('Fehler:', error);
        alert('Abmeldung fehlgeschlagen, bitte versuchen Sie es erneut');
    }
});
